
@set MSBuild="%ProgramFiles(x86)%\MSBuild\14.0\Bin\MSBuild.exe"
@if not exist %MSBuild% @set MSBuild="%ProgramFiles%\MSBuild\14.0\Bin\MSBuild.exe"
@REM .nuget/nuget.exe  restore

@REM %MSBuild% Dapper.sln /v:m
cd  Dapper
dotnet restore
dotnet build -o ../bin -f net451
dotnet build -o ../bin/net46 -f net46
dotnet build -o ../bin/dotnet54 -f dotnet54
cd ..

@PAUSE